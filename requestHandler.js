var querystring = require("querystring");
var fs = require("fs")
var formidable = require("formidable");

function start(resp){
  console.log("Request handler 'start' was called.")

  var body = '<html>'+
  '<head>'+
    '<meta http-equiv="Content-Type" content="text/html; '+
    'charset=UTF-8" />'+
  '</head>'+
  '<body>'+
    '<form action="/upload" enctype="multipart/form-data" method="post">'+
      '<input type="file" name="upload" multiple="multiple"></input>'+
      '<input type="submit" value="Upload file" />'+
    '</form>'+
  '</body>'+
  '</html>';

  resp.writeHead(200, {"Content-type": "text/html"})
  resp.write(body)
  resp.end()
}

function upload(resp, request){
  console.log("Request handler 'upload' was called.")
 
  var form = new formidable.IncomingForm()
  console.log("about to parse")
  form.parse(request, function(error, fields, files){
    console.log("parsing done.")

    fs.renameSync(files.upload.path, "/tmp/test.png");

    resp.writeHead(200, {"Content-Type": "text/html"});
    resp.write("received image:<br/>");
    resp.write("<img src='/show' />");
    resp.end();
  })
}

function show(resp){
  console.log("Request handler 'show' was called.")
 
  fs.readFile("/tmp/test.png", "binary", function(error, file){
    if(error){
      resp.writeHead(500, {"Content-type": "text/plain"})
      resp.write("error:" + error + "\n")
      resp.end()
    }else{
      resp.writeHead(200, {"Content-type": "image/png"})
      resp.write(file, "binary")
      resp.end()
    }
  })
}

exports.start = start
exports.upload = upload
exports.show = show
