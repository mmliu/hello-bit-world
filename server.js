var http = require("http")
var url = require("url");

function start(route, handlers){

  var onRequest = function(request, response){
    var pathname = url.parse(request.url).pathname
    console.log("Request for " + pathname + " received.")

    route(pathname, handlers, response, request)
  }

  http.createServer(onRequest).listen(8888)
  console.log("server has started")
}

exports.start = start
