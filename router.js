function route(pathname, handlers, resp, request){
  console.log("About to route a request for " + pathname)

  if(typeof handlers[pathname] === "function")
     return handlers[pathname](resp, request)
  else{
     console.log("No request handler found for " + pathname);

     resp.writeHead(200, {"Content-Type": "text/plain"});
     resp.write("404 Not found")
     resp.end()
  }
}

exports.route=route
